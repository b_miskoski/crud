 <!-- Include header -->
 <?php include 'inc/header.php'; ?>

    <!-- Jumbotron greatings/info -->
    <div class="container">
        <div class="jumbotron text-center">
            <h1 class="display-3">Welcome</h1>
            <p class="lead">Please enter your personal information</p>
        </div>
    </div>

    <!-- Grid of 8 by 4 containing insert form and additional info -->
    <div class="container">
        <div class="row">
          <form class="col-md-8 col-sm-8 col-8" id="userform" name="form" action="process.php" method="post">
            <div class="form-group">
                <label>Name *</label>
                <input type="text" id="first_name" name="first_name" class="form-control" value="" placeholder="Enter your name">
            </div>

            <div class="form-group">
                <label>Last name *</label>
                <input type="text" id="last_name" name="last_name" class="form-control" value="" placeholder="Enter your last name" >
            </div>

            <div class="form-group">
                <label>Email *</label>
                <input type="email" id="email" name="email" class="form-control" value="" placeholder="Enter your email" >
            </div>

            <label>Phone number *</label>
            <div>
                <table class="table" id="dynamic_field">
                    <tr class="row">
                        <td><input type="tel" placeholder="+389" class="form-control text-center" disabled></td>
                        <td class="col-md"><input type="tel" name="phone[]" placeholder="Enter your current phone number" class="form-control" /></td>
						<td><button type="button" name="add_num" id="add_num" class="btn btn-primary">+ </button></td>
                    </tr>
                </table>
            </div>

            <button type="submit" class="btn btn-outline-primary col-lg" name="submit">Submit</button>
            </form>

            <div class="col-md-4 col-sm-4 col-4">
              <p>Please fill out all of the form fields marked as required (*). Fill out the form with truthful, and up to date informations.</p>
              <a href="index.php" class="btn btn-outline-success col lg">View users</a>
            </div>
        </div>
      </div>

 <!-- Include footer -->
<?php include 'inc/footer.php'; ?>