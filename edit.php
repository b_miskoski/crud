 <!-- Include header -->
<?php include 'inc/header.php'; ?>

<!-- Jumbotron greatings/info -->
    <div class="container">
        <div class="jumbotron text-center">
            <h1 class="display-4">Edit user</h1>
            <p class="lead">Please edit your personal information</p>
        </div>
    </div>

<?php include 'process.php'; ?>

<!-- Form that stores the data for update -->
    <div class="container">
        <div class="row">
            <form class="col-md-8 col-sm-8 col-8" id="userform" name="form" action="process.php" method="post">
                <div class="form-group">
                    <label>Name *</label>
                    <input type="text" id="first_name" name="first_name" class="form-control" value="<?php echo $first_name; ?>" placeholder="Enter your name">
                </div>

                <div class="form-group">
                    <label>Last name *</label>
                    <input type="text" id="last_name" name="last_name" class="form-control" value="<?php echo $last_name; ?>" placeholder="Enter your last name" >
                </div>

                <div class="form-group">
                    <label>Email *</label>
                    <input type="email" id="email" name="email" class="form-control" value="<?php echo $email; ?>" placeholder="Enter your email" >
                </div>

                <label>Phone number *</label>
                <div class="form-group">
                    <table class="table" id="dynamic_field">
                        <tr>
                            <?php foreach ($result2 as $key => $value): ?>
                                <?php foreach ($value as $phone2 => $phone): ?>
                                    <tr class="row">
                                        <td><input type="tel" placeholder="+389" class="form-control text-center" disabled></td>
                                        <td class="col-md"><input type="tel" name="phone[]" placeholder="" class="form-control" value="<?php echo $phone; ?>" ></td>
                                     
                                    </tr>
                                <?php endforeach ?>
                            <?php endforeach ?>
                            <td><button type="button" name="add_num" id="add_num" class="btn btn-primary col-md-8 col-sm-8 col-8">Add more numbers </button></td>
                        </tr>
                    </table>
                </div>

                <input type="hidden" name="update_id" value="<?php echo $id; ?>">
                <button type="submit" class="btn btn-outline-primary col-lg" name="update">Update</button>
            </form>
            
<!-- Include footer -->
<?php include 'inc/footer.php'; ?>