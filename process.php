<?php
    // Starting a session for displaying session messagess 
    session_start();

    // Require the config.php file for the defined constants 
    require 'config/config.php';

    // Makes connection to the database
    $conn = @mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME) or die($conn->error);

    // Checks if the submit button has been pressed and enters data in the tables
    if(isset($_POST['submit'])){

        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];

        // Query for inserting data in first table
        $sql = "INSERT INTO personal (first_name, last_name, email) VALUES ('$first_name', '$last_name', '$email')" OR die($mysqli->error);
        mysqli_query($conn, $sql);
        
        // Geting the last entered id and adding it to a variable
        $user_id = $conn->insert_id;

        // Looping trough the array and using query to insert the data in the phone table
        foreach($_POST['phone'] as $phone){
            $sql1 = "INSERT INTO phone (user_id, phone) VALUES ('$user_id', '$phone')" OR die($mysqli->error);
            mysqli_query($conn, $sql1);
        }

        // Sessions messages, and message types
        $_SESSION['message'] = "Record has been saved!";
        $_SESSION['msg_type'] = "success"; 

        header("Location: index.php");
    }

    // Checks if the delete button has been pressed and deletes the matching id
    if (isset($_GET['delete'])){
        $id = $_GET['delete'];
        $conn->query("DELETE FROM personal WHERE id=$id") or die($conn->error());

        // Sessions messages, and message types
        $_SESSION['message'] = "Record has been deleted!";
        $_SESSION['msg_type'] = "danger";
   }

   // Checks if the edit button has been pressed
    if (isset($_GET['edit'])) {
        $id = $_GET['edit'];

        // Query that selects all the data from the table personal
        $result = $conn->query("SELECT * FROM personal where id=$id") or die($conn->error);

        // While loop that loops trought the $result variable and adds the value to the $row variable and later to the other variables from the first table
        while ($row = mysqli_fetch_assoc($result)) {

            $id = $row['id'];
            $first_name = $row['first_name'];
            $last_name = $row['last_name'];
            $email = $row['email'];

            // Second query that selects the data from the phone table
            $result2 = $conn->query("SELECT phone.phone FROM phone where user_id=$id") or die($conn->error);

        }   

        // While loop that loops trought the $result variable and adds the value to the $row variable and adds data to the phone variable
        while ($row2 = mysqli_fetch_assoc($result2)) {
            $phone = $row2['phone'];
        }
    }

    // Checks if the update button has been pressed, deletes the data and enter the new data
    if (isset($_POST['update'])) {

        $update_id = $_POST['update_id'];
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];   

        // Query for deleting the data from the tables/ using the on delete cascade we are able to delete both tables with one query
        $conn->query("DELETE FROM personal WHERE id=$update_id") or die($conn->error());

        // Second query that enters the data in the personal table
        $sql = "INSERT INTO personal (id, first_name, last_name, email) VALUES ('$update_id','$first_name', '$last_name', '$email')" OR die($mysqli->error);
        mysqli_query($conn, $sql);

        foreach($_POST['phone'] as $phone){
            // Third query that adds data to the phone table
            $sql1 = "INSERT INTO phone (user_id, phone) VALUES ('$update_id', '$phone')" OR die($mysqli->error);
            mysqli_query($conn, $sql1);

        // Sessions messages, and message types
        $_SESSION['message'] = "Record has been updated!";
        $_SESSION['msg_type'] = "info";

        header("Location: index.php");
        }
    }

    