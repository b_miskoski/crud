      
      // ADDING ADITIONAL PHONE NUMBER FIELDS
      $(document).ready(function(){
        var i=1;
        $('#add_num').click(function(){
          i++;
          $('#dynamic_field').append('<tr id="row'+i+'" class="row"><td><input type="tel" placeholder="+389" class="form-control text-center" disabled></td><td class="col-md"><input type="tel" name="phone[]" placeholder="Enter additional phone number" class="form-control" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">x</button></td></tr>');
          });
        
          $(document).on('click', '.btn_remove', function(){
            var button_id = $(this).attr("id"); 
            $('#row'+button_id+'').remove();
          });
      });

      // FORM VALIDATION 
      $(document).ready(function(){
        
        // VALIDATOR METHOD THAT ACCEPTS ONLY LETTER (NO SPECIAL CARACTERS FOR NAME AND LAST NAME)
        $.validator.addMethod("lettersOnly", function(value, element) {
            return this.optional(element) || /^[a-z\u0400-\u04FF]+$/i.test(value);
          }, "Letters only please");
    
        // VALIDATOR METHOD THAT ACCEPTS ONLY NUMBERS FROM 0-9 AND MAX LENGHT OF 8 CHARS 
        $.validator.addMethod('positiveNumber',function (value, element) {
            return this.optional(element) || /^\d{8}$/.test(value);
        }, "Please enter valid Macedonian based number, no blank spaces");

        // SPECIAL VALIDATION FOR DYNAMICALLY ADDED FIELDS 
        $.validator.prototype.checkForm = function() {
            this.prepareForm();
            for (var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++) {
                if (this.findByName(elements[i].name).length !== undefined && this.findByName(elements[i].name).length > 1) {
                    for (var cnt = 0; cnt < this.findByName(elements[i].name).length; cnt++) {
                        this.check(this.findByName(elements[i].name)[cnt]);
                    }
                } else {
                    this.check(elements[i]);
                }
            }
            return this.valid();
        };

        // VALIDATION RULES FOR THE FORM FIELDS AND CUSTOM MESSAGES FOR INVALID INPUTS
          $("#userform").validate({
            errorClass: "is-invalid text-danger",
            validClass: "is-valid",
            rules: {
              first_name: {
                required: true,
                lettersOnly: true
              },
              last_name: {
                required: true,
                lettersOnly: true
              },
              email: {
                required: true,
                email: true
              },
              'phone[]': {
                required: true,
                positiveNumber: true,
              }
            },
            messages: {
              first_name: {
                required: "Please enter your name!",
                lettersOnly: "Letters only please, no blank space allowed"
              },
              last_name: {
                required: "Please enter your last name!",
                lettersOnly: "Letters only please, no blank space allowed"
              },
              email: {
                required: "Please enter your email address"
              },
              phone_number: {
                required: "Please enter your phone number"
              }
            }
          });
        });