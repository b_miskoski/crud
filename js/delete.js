// DELETE CONFIRMATION
$(document).ready(function(){
    $(function() {
      $('td a#delete').click(function() {
        return confirm("Are you sure that you want to delete this record?");
      });
    });
  });