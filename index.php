     <!-- Include header -->
    <?php include 'inc/header.php'; ?>

    <!-- Jumbotron greatings/info -->
        <div class="container">
            <div class="jumbotron text-center">
                <h1 class="display-4">Entry results</h1>
                <hr style="height:1px;border:none;color:#333;background-color:#333;">
                <p class="lead">The information from the entry results are displyed in the table bellow.</p>
                <p class="lead">Here you can edit or delete the user data.</p>
            </div>
        </div>

    <?php
        // Requires the process.php file that contains the db connection
        include 'process.php';

        // Query for presenting data in a table, also for grouping the additional phone numbers for the users who have more then one phone number
        $result = $conn->query("SELECT personal.id, personal.first_name, personal.last_name, personal.email, GROUP_CONCAT(phone) as phone FROM personal LEFT JOIN phone ON personal.id = phone.user_id GROUP BY personal.id") or die($conn->error);
    ?>

    <!-- Session messages that are displaying if a record has been entered, deleted or updated -->
    <?php if (isset($_SESSION['message'])): ?>

    <div class="alert alert-<?=$_SESSION['msg_type']?> text-center container">

          <?php
            echo $_SESSION['message'];
            unset($_SESSION['message']);
          ?>
    </div>
    <?php endif ?>
    
    <!--Table for data presentation -->
        <div class="container table-hover table-sm">
            <table class="table">
                <tr class="thead-dark">
                    <th>No.<th>
                    <th>Full name</th>
                    <th>Email</th>
                    <th>Phone number(s)</th>
                    <th colspan="2" class="text-left">Actions</th>
                </tr>

    <?php
        // Adding the data from the $result variable to the $row variable
        while ($row = $result->fetch_assoc()):
    ?>
            <!-- Row that prints out the data in the table -->
                 <tr>
                    <!-- Printing the data in the table -->
                    <td><?php echo $row['id'];?></td>
                    <!-- Blank <td> for table aligment -->
                    <td></td>
                    <td><?php echo $row['first_name'];?> <?php echo $row['last_name'];?></td>
                    <td><?php echo $row['email'];?></td>
                    <td><?php echo $row['phone']?></td>
                    <td>
                        <a href="edit.php?edit=<?php echo $row['id']; ?>"
                        class="btn btn-info">Edit</a>
                        <a href="index.php?delete=<?php echo $row['id']; ?>"
                        class="btn btn-danger" id="delete">Delete</a>
                     </td>
                </tr>
    <?php
        endwhile;
    ?>
            </table>
        </div>

     <!-- Include footer -->        
    <?php include 'inc/footer.php'; ?>